package org.example.api.controller;

import org.example.domain.model.Endereco;
import org.example.domain.model.Pessoa;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;

@ExtendWith(MockitoExtension.class)
public class PessoaControllerTest {

    @Test
    void buscaPeloCodigo(){
        PessoaController mock = Mockito.mock(PessoaController.class);
        Assert.assertTrue(mock.buscarPeloCodigo(1L).isEmpty());
    }

    @Test
    void adicionar(){
        PessoaController mock = Mockito.mock(PessoaController.class);

        Assert.assertNull(mock.adicionar(retornaPessoa()));
    }

    @Test
    void atualizar(){
        PessoaController mock = Mockito.mock(PessoaController.class);
        Assert.assertNull(mock.atualizar(1L, retornaPessoa()));
    }

    @Test
    void pesquisar(){
        PessoaController mock = Mockito.mock(PessoaController.class);
        Assert.assertTrue(mock.pesquisar("teste").isEmpty());
    }

    Pessoa retornaPessoa(){
        Pessoa pessoa = new Pessoa();
        Endereco endereco = new Endereco();

        endereco.setCep("650000");
        endereco.setLogradouro("teste");
        endereco.setNumero("1234");
        endereco.setCidade("Sao luis");

        LocalDateTime hj = LocalDateTime.now();

        pessoa.setCodigo(1L);
        pessoa.setNome("Teste");
        pessoa.setDataNascimento(hj);
        pessoa.setEndereco(endereco);

        return pessoa;
    }

}
