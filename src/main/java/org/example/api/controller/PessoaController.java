package org.example.api.controller;

import org.example.repository.PessoaRepository;
import org.example.domain.model.Pessoa;
import org.example.domain.service.PessoaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/pessoas")
public class PessoaController {

    @Autowired
    private PessoaRepository pessoaRepository;

    @Autowired
    private PessoaService pessoaService;

    @GetMapping("/{codigo}")
    public Optional<Pessoa> buscarPeloCodigo(@PathVariable Long codigo){
        return pessoaRepository.findById(codigo);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Pessoa adicionar(@RequestBody @Valid Pessoa pessoa){
        return pessoaRepository.save(pessoa);
    }

    @DeleteMapping("/{codigo}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remover(@PathVariable Long codigo){
        pessoaService.remover(codigo);
    }

    @PutMapping("/{codigo}")
    public Pessoa atualizar(@PathVariable Long codigo, @Valid @RequestBody Pessoa pessoa){
        return pessoaService.atualizar(codigo, pessoa);
    }

    @GetMapping
    public List<Pessoa> pesquisar(@RequestParam(required = false, defaultValue = "") String nome) {
        return pessoaRepository.findByNomeContaining(nome);
    }

}
