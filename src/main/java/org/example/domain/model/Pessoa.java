package org.example.domain.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Data
@Entity
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Pessoa {

    @EqualsAndHashCode.Include
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long codigo;

    @NotBlank
    @Column(nullable = false)
    @NotNull
    @Size(min = 3, max = 20)
    private String nome;

    @NotNull
    @Column(name = "data_nascimento")
    private LocalDateTime dataNascimento;

    @Embedded
    private Endereco endereco;
}