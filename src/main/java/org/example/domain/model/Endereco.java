package org.example.domain.model;

import lombok.Data;

import javax.persistence.Embeddable;

@Data
@Embeddable
public class Endereco {

    private String logradouro;
    private String cep;
    private String numero;
    private String cidade;
}
