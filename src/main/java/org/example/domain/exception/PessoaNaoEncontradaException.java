package org.example.domain.exception;

public class PessoaNaoEncontradaException extends EntidadeNaoEncontradaException {
    public static final long serialVersionUID = 1L;
    public PessoaNaoEncontradaException(String message){
        super(message);
    }
    public PessoaNaoEncontradaException(Long pessoaId){
        this(String.format("Pessoa inativa ou não existe um cadastro de pessoa com código %d", pessoaId));
    }
}
