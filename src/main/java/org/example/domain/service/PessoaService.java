package org.example.domain.service;

import org.example.domain.exception.EntidadeEmUsoException;
import org.example.domain.exception.PessoaNaoEncontradaException;
import org.example.domain.model.Pessoa;
import org.example.repository.PessoaRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PessoaService {

    @Autowired
    private PessoaRepository pessoaRepository;

    private static final String MSG_PESSOA_EM_USO
            = "Pessoa de código %d não pode ser removida, pois está em uso";

    @Transactional
    public void remover(Long codigo){
        try {
            pessoaRepository.deleteById(codigo);
        } catch (EmptyResultDataAccessException ex) {
            throw new PessoaNaoEncontradaException(codigo);
        } catch (DataIntegrityViolationException ex){
            throw new EntidadeEmUsoException(String.format(MSG_PESSOA_EM_USO, codigo));
        }
    }

    public Pessoa buscarPeloCodigo(Long pessoaId) {
        return pessoaRepository.findById(pessoaId)
                .orElseThrow(() -> new PessoaNaoEncontradaException(pessoaId));
    }

    @Transactional
    public Pessoa atualizar(Long codigo, Pessoa pessoa){
        Pessoa pessoaSalva = buscarPeloCodigo(codigo);
        BeanUtils.copyProperties(pessoa, pessoaSalva, "codigo");
        return pessoaRepository.save(pessoaSalva);
    }


}
